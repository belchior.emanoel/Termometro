#include <iostream>
#include "fahrenheit.hpp"
#include <string>

using namespace std;

Fahrenheit::Fahrenheit(){
}

Fahrenheit::Fahrenheit(float temperatura):Termometro(temperatura){
}

Fahrenheit::~Fahrenheit(){
}

void Fahrenheit::calculaTemperatura(){
	float fahrenheit = getTemperatura();
	float kelvin, celsius;
	kelvin = 273 + ((5/9)*(fahrenheit+32));
	celsius = (5/9)*(fahrenheit - 32);
}
