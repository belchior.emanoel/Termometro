#ifndef FAHRENHEIT_H
#define KELVIN_H

#include "termometro.hpp"
#include <string>
#include <iostream>

using namespace std;

class Fahrenheit:public Termometro {

	public:

		Fahrenheit();
		Fahrenheit(float temperatura);
		~Fahrenheit();
		void calculaTemperatura();
};
#endif
