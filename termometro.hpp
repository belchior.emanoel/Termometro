#ifndef TERMOMETRO_H
#define TERMOMETRO_H
#include <string>
#include <iostream>


class Termometro {

private:

	float temperatura;
public:
	Termometro();
	Termometro(float temperatura);
	~Termometro();

	void setTemperatura(float temperatura);
	float getTemperatura();
	void calculaTemperatura();	
};
#endif
