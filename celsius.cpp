#include <iostream>
#include "celsius.hpp"
#include <string>

using namespace std;

Celsius::Celsius(){
}

Celsius::Celsius(float temperatura):Termometro(temperatura){

}
Celsius::~Celsius(){
}

void Celsius::calculaTemperatura(){
	float celsius = getTemperatura();
	float kelvin, fahrenheit;
	kelvin = celsius + 273;
	fahrenheit = 32+(celsius * 9 / 5);

	cout << "Conversao em Kelvin: " << kelvin << endl;
	cout << "Conversao em Fahrenheit " << fahrenheit << endl;
}


