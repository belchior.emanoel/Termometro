#ifndef CELSIUS_H
#define CELSIUS_H

#include "termometro.hpp"
#include <string>
#include <iostream>


class Celsius:public Termometro {

	public:
		Celsius();
		Celsius(float temperatura);
		~Celsius();
		void calculaTemperatura();
};
#endif
