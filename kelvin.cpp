#include <iostream>
#include "kelvin.hpp"
#include <string>

using namespace std;

Kelvin::Kelvin(){
}

Kelvin::Kelvin(float temperatura):Termometro(temperatura){
}

Kelvin::~Kelvin(){
}

void Kelvin::calculaTemperatura(){
	float kelvin = getTemperatura();
	float celsius, fahrenheit;
	celsius = kelvin-273;
	fahrenheit = 32+(celsius * 9 /5);

	cout << "Conversao em Celsius: " << celsius << endl;
	cout << "Conversao em Fahrenheit: " << fahrenheit << endl;

}


