#ifndef KELVIN_H
#define KELVIN_H

#include "termometro.hpp"
#include <string>
#include <iostream>

using namespace std;

class Kelvin:public Termometro {

	public:

		Kelvin();
		Kelvin(float temperatura);
		~Kelvin();
		void calculaTemperatura();			
};
#endif
