#include "celsius.hpp"
#include "fahrenheit.hpp"
#include "kelvin.hpp"
#include "termometro.hpp"
#include <iostream>
#include <string>

using namespace std;

int main(){
int opcao;
float medida=0;
cout << "Escolha a temperatura desejada: 1 - Kelvin \n2 - Fahrenheit\n3 - Celsius : " << endl;
cin >> opcao;

Fahrenheit * objetoFahrenheit;
Celsius * objetoCelsius;

switch(opcao){
case (1):
	cout << "Digite a temperatura: " << endl;
	cin >> medida;
	Kelvin * objetoKelvin = new Kelvin(medida);
	objetoKelvin->calculaTemperatura();
	delete(objetoKelvin);
	break;
case(2):
	cout << "Digite a temperatura: " << endl;
	cin >> medida;
	objetoFahrenheit = new Fahrenheit(medida);
	objetoFahrenheit->calculaTemperatura();
	delete(objetoFahrenheit);
	break;
case(3):
	cout << "Digite a temperatura: " << endl;
	cin >> medida;
	objetoCelsius = new Celsius(medida);
	objetoCelsius->calculaTemperatura();
	delete(objetoCelsius);
	break; 
}

return 0;
}

